package com.mkath.demoimagerecognition

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.AttributeSet
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnEntry.setOnClickListener {
            val intent = Intent(applicationContext, BuyTicketActivity::class.java)
            startActivity(intent)
        }

        btnOrganizer.setOnClickListener {
            val intent = Intent(applicationContext, VerificateActivity::class.java)
            startActivity(intent)
        }
    }

}
