package com.mkath.demoimagerecognition

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker

class PermissionHelper {

    companion object {
        private const val REQUEST_EXTERNAL_STORAGE = 42
    }

    fun askForPermissions(
        activity: Activity,
        permission: String
    ): Boolean {
        @PermissionChecker.PermissionResult
        val permissionCheck =
            ContextCompat.checkSelfPermission(activity.applicationContext, permission)
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                REQUEST_EXTERNAL_STORAGE
            )
        } else {
            return true
        }
        return false
    }

    fun handlePermissionResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray, listener: ((result: Boolean) -> Unit)
    ) {
        if (requestCode == REQUEST_EXTERNAL_STORAGE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                listener.invoke(true)
            } else {
                listener.invoke(false)
            }
        }
    }
}