package com.mkath.demoimagerecognition.data.network
import com.google.gson.annotations.SerializedName


class BodyVerificateResponse(
    @SerializedName("details")
    val details: ArrayList<DataVerificateResponse>
)
