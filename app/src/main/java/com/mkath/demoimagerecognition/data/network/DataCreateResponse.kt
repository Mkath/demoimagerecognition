package com.mkath.demoimagerecognition.data.network
import com.google.gson.annotations.SerializedName


class DataCreateResponse(
    @SerializedName("msg")
    val msg: String
)
